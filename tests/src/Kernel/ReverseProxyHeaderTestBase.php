<?php

namespace Drupal\Tests\reverse_proxy_header\Kernel;

use Composer\Autoload\ClassLoader;
use Drupal\Core\Database\Database;
use Drupal\Core\DrupalKernel;
use Drupal\Core\Logger\LogMessageParser;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Site\Settings;
use Drupal\KernelTests\KernelTestBase;
use Drupal\dblog\Logger\DbLog;
use Drupal\reverse_proxy_header\EventSubscriber\ReverseProxyHeaderClientIpRestore;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Creates abstract base class for Reverse Proxy Header tests.
 */
abstract class ReverseProxyHeaderTestBase extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['dblog'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('dblog', ['watchdog']);
  }

  /**
   * Base code for ReverseProxyHeaderClientIpRestoreTest tests.
   *
   * @param string $header
   *   The test header.
   * @param string $expected_client_ip
   *   The expected clientIp address.
   *
   * @return \Symfony\Component\HttpFoundation\Request
   *   Return the request object.
   */
  public function reverseProxyHeaderRequest(
    string $header,
    string $expected_client_ip,
  ): Request {
    $auto_loader = new ClassLoader();
    $request = Request::create('test');
    $site_path = DrupalKernel::findSitePath($request);
    $app_root = dirname(substr(__DIR__, 0, -strlen(__NAMESPACE__)), 2);
    Settings::initialize($app_root, $site_path, $auto_loader);

    // Set server parameter reverse_proxy_header as expected client ip.
    $request->server->set($header, $expected_client_ip);
    $logger = (new LoggerChannelFactory(
      $this->createMock(RequestStack::class),
      $this->createMock(AccountInterface::class),
    ))->get('reverse_proxy_header');
    $logger->addLogger(new DbLog(Database::getConnection(), new LogMessageParser()));
    $reverse_proxy_header_client_ip_restore_service = new ReverseProxyHeaderClientIpRestore(new Settings(Settings::getAll() + ['reverse_proxy_header' => $header]), $logger);

    $kernel = $this->createMock('Symfony\Component\HttpKernel\HttpKernelInterface');
    $event = new RequestEvent($kernel, $request, HttpKernelInterface::MAIN_REQUEST);
    $reverse_proxy_header_client_ip_restore_service->onRequest($event);
    return $request;
  }

  /**
   * Checks if the notice exist for current invalid IP address.
   *
   * @param string $header_name
   *   The header name.
   */
  protected function assertInvalidIpNoticeExist(string $header_name): void {
    // Prepares the message and its arguments for testing.
    $message = 'No valid IP address found in the @header_name header.';
    $arguments = ['@header_name' => $header_name];
    $variables = (new LogMessageParser())->parseMessagePlaceholders($message, $arguments);

    // Select the expected notice from the database.
    $query = Database::getConnection()->select('watchdog');
    $query
      ->fields('watchdog', ['wid'])
      ->condition('severity', RfcLogLevel::NOTICE)
      ->condition('type', 'reverse_proxy_header')
      ->condition('message', $message)
      ->condition('variables', serialize($variables));
    $count = (int) $query->countQuery()->execute()->fetchField();

    $this->assertSame($count, 1);
  }

}
