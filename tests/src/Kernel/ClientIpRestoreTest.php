<?php

namespace Drupal\Tests\reverse_proxy_header\Kernel;

/**
 * Test class for ReverseProxyHeaderClientIpRestore service.
 *
 * @group reverse_proxy_header
 */
class ClientIpRestoreTest extends ReverseProxyHeaderTestBase {

  /**
   * Test ReverseProxyHeaderClientIpRestore service using valid IP addresses.
   *
   * @param string $header_name
   *   The header name.
   * @param string $header_value
   *   The header value.
   * @param string $valid_ip
   *   The valid expected client IP address.
   *
   * @dataProvider providerClientValidIps
   */
  public function testValidIps(
    string $header_name,
    string $header_value,
    string $valid_ip,
  ): void {
    $request = $this->reverseProxyHeaderRequest($header_name, $header_value);
    $this->assertEquals($request->getClientIp(), $valid_ip);
  }

  /**
   * Test ReverseProxyHeaderClientIpRestore service using invalid IP addresses.
   *
   * @param string $header_name
   *   The header name.
   * @param string $header_value
   *   The header value.
   *
   * @dataProvider providerClientInvalidIps
   */
  public function testInvalidIps(
    string $header_name,
    string $header_value,
  ): void {
    $this->reverseProxyHeaderRequest($header_name, $header_value);
    $this->assertInvalidIpNoticeExist($header_name);
  }

  /**
   * Provider for testing ClientIpRestoreTest::testValidIps.
   *
   * @return array
   *   Test Data to simulate incoming IP address.
   */
  public static function providerClientValidIps(): array {
    return [
      [
        // It should work well with default headers as well.
        'header_name' => 'HEADER_X_FORWARDED_FOR',
        'header_value' => '127.0.0.1',
        'valid_ip' => '127.0.0.1',
      ],
      [
        'header_name' => 'HEADER_X_CUSTOM_HEADER_NAME',
        'header_value' => '8.8.8.8',
        'valid_ip' => '8.8.8.8',
      ],
      [
        'header_name' => 'HEADER_WITH_ONE_INVALID_IP',
        'header_value' => '127.0.0.1,not-an-ip,127.0.0.2',
        'valid_ip' => '127.0.0.1',
      ],
      [
        'header_name' => 'HEADER_WITH_INVALID_IP_FIRST',
        'header_value' => 'not-an-ip,10.0.10.10',
        'valid_ip' => '10.0.10.10',
      ],
    ];
  }

  /**
   * Provider for testing ClientIpRestoreTest::testInvalidIps.
   *
   * @return array
   *   Test Data to simulate incoming IP address.
   */
  public static function providerClientInvalidIps(): array {
    return [
      [
        'header_name' => 'HEADER_X_FORWARDED_FOR',
        'header_value' => '444.555.666.777',
      ],
      [
        'header_name' => 'HEADER_X_CUSTOM_HEADER_2',
        'header_value' => 'this_is_a_text_not_ip',
      ],
      [
        'header_name' => 'HEADER_WITH_ALL_INVALID_IPS',
        'header_value' => 'some-value,444.555.666.777',
      ],
    ];
  }

}
