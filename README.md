# Reverse Proxy Header

This module is the simplest way to use the specific HTTP header name
to determine the client IP. The most common usage is:
- real client IP is stored inside a non-default header HTTP_X_FORWARDED_FOR;
- you have no ability to affect this on reverse proxy or server sides.

___

The module provides an equivalent of `reverse_proxy_header` setting (which is
deprecated from Drupal 8.7.0). @see https://www.drupal.org/node/3030558

How to use it:
- Step 1) Install and enable the module.
- Step 2) Add the `reverse_proxy_header` setting to your setting.php file:
```
/**
 * Set the HTTP header name which stores real client IP.
 */
$settings['reverse_proxy_header'] = 'HTTP_X_FORWARDED_FOR_CUSTOM_HEADER';
```

___

What about available alternatives?
- rename the header name (or copy its value) to the supported header
  HTTP_X_FORWARDED_FOR on proxy or server side;
- copy the value from the custom header to $_SERVER['HTTP_X_FORWARDED_FOR']
  in your index.php before Drupal initialization.
