<?php

namespace Drupal\reverse_proxy_header\EventSubscriber;

use Drupal\Core\Site\Settings;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Restores the true client IP address.
 */
class ReverseProxyHeaderClientIpRestore implements EventSubscriberInterface {

  /**
   * The settings object.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected Settings $settings;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Constructs a ReverseProxyHeaderClientIpRestore.
   *
   * @param \Drupal\Core\Site\Settings $settings
   *   The settings object.
   * @param \Psr\Log\LoggerInterface $logger
   *   The reverse_proxy_header logger channel.
   */
  public function __construct(
    Settings $settings,
    LoggerInterface $logger,
  ) {
    $this->settings = $settings;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    // Priority is set to 350 (to run before RouterListener::onKernelRequest).
    // Which allows AuthenticationSubscriber methods to use the request IP.
    // @see \Symfony\Component\HttpKernel\EventListener\RouterListener::getSubscribedEvents
    // @see \Drupal\Core\EventSubscriber\AuthenticationSubscriber::getSubscribedEvents
    // @see https://www.drupal.org/project/reverse_proxy_header/issues/3290953
    $events[KernelEvents::REQUEST][] = ['onRequest', 350];
    return $events;
  }

  /**
   * Restores the origination client IP from the custom HTTP header.
   */
  public function onRequest(RequestEvent $event): void {

    // Checks the available settings.
    $reverse_proxy_header_name = $this->settings->get('reverse_proxy_header');
    if (!$reverse_proxy_header_name) {
      return;
    }

    // Checks the header value.
    $connecting_ips = $event->getRequest()->server->get($reverse_proxy_header_name);
    if (empty($connecting_ips)) {
      return;
    }

    // Extracts the first valid IP address from the header.
    $connecting_ips = array_map('trim', explode(',', $connecting_ips));
    foreach ($connecting_ips as $connecting_ip) {
      if (!$this->isInvalidIpAddress($connecting_ip, $reverse_proxy_header_name)) {

        // As the changed remote address will make it impossible to determine
        // a trusted proxy, we need to make sure we set the right protocol.
        // @see \Symfony\Component\HttpFoundation\Request::isSecure()
        $event->getRequest()->server->set('HTTPS', $event->getRequest()->isSecure() ? 'on' : 'off');
        $event->getRequest()->server->set('REMOTE_ADDR', $connecting_ip);
        $event->getRequest()->overrideGlobals();
        return;
      }
    }

    $this->logger->notice('No valid IP address found in the @header_name header.', ['@header_name' => $reverse_proxy_header_name]);
  }

  /**
   * Check if IP address is invalid.
   *
   * @param string $ip_address
   *   Ip address.
   * @param string $header_name
   *   The header name.
   *
   * @return bool
   *   Return TRUE if IP address is invalid.
   */
  protected function isInvalidIpAddress(
    string $ip_address,
    string $header_name,
  ): bool {
    $variables = [
      '@ip_address' => $ip_address,
      '@header_name' => $header_name,
    ];

    if (empty($ip_address)) {
      $this->logger->notice('Empty IP address value retrieved from @header_name header.', $variables);
      return TRUE;
    }

    if (!filter_var($ip_address, FILTER_VALIDATE_IP)) {
      $this->logger->notice('IP address `@ip_address` retrieved from @header_name header is invalid.', $variables);
      return TRUE;
    }

    return FALSE;
  }

}
